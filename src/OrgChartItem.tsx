import React, {Component} from 'react';
import {IDepartment} from './Api';

import {
  FaCaretDown,
  FaCaretRight,
  FaRegSquare,
  FaRegCheckSquare,
} from 'react-icons/fa';
import {css} from 'emotion';
import classnames from 'classnames';

type CollapsedState = {
  collapsed: Boolean;
};



export interface OrgChartItemProps {
  tree: IDepartment
  onChange: Function
  value: IDepartment
  collapsed?: Boolean
  level: number
}


export default class OrgChartItem extends Component<
  OrgChartItemProps,
  CollapsedState
> {
  static defaultProps = {
    collapsed: true,
    level: 0,
  };

  constructor(props: OrgChartItemProps, state: CollapsedState) {
    super(props, state);
    this.state = {
      collapsed: props.collapsed ? props.collapsed : false,
    };
  }

  toggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  isSelected():Boolean {
    return this.props.value.id === this.props.tree.id
  }

  select() {
    if (!this.isSelected()) {
      this.props.onChange(this.props.tree);
    }
  }

  render() {
    const leadClass = classnames({
      [styles.button]: true,
      [styles.active]: this.isSelected(),
    });

    return (
      <div style={{marginLeft: `${15 * this.props.level}px`}}>
        {this.props.tree.children.length > 0 ? (
          <button onClick={() => this.toggle()} className={styles.button}>
            {this.props.tree.name}
            {this.state.collapsed ? <FaCaretRight /> : <FaCaretDown />}
          </button>
        ) : (
          <button
            onClick={() => {
              this.select();
            }}
            className={leadClass}>
            {this.isSelected() ? (<FaRegCheckSquare className={styles.icon}/>) : (<FaRegSquare  className={styles.icon}/>)}
            {this.props.tree.name}
          </button>
        )}
        {this.props.tree.children.length > 0 && !this.state.collapsed
          ? this.props.tree.children.map((el, key: number) => (
              <OrgChartItem
                key={key}
                value={this.props.value}
                onChange={this.props.onChange}
                tree={el}
                level={this.props.level + 1}
              />
            ))
          : ''}
      </div>
    );
  }
}

const styles = {
  button: css`
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1);
    font-family: system-ui;
    padding: 4px;
    border: 1px solid #ebebeb;
    background-color: white;
    color: black;
    cursor: pointer;
    &:hover {
        text-decoration: underlined;
    }
  `,
  active: css`
    background-color: lightblue;
  `,
  icon: css`
    font-size: 16px; 
    margin-right: 10px;
  `
};
