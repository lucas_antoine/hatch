
export const emptyDepartment: IDepartment = {
  id: -1,
  name: 'empty',
  children: [],
};

export interface IDepartment {
  id: number
  name: string
  children: IDepartment[]
}
