import {IDepartment, emptyDepartment} from './Api';


export const filterOrgChartTree = (input : IDepartment, topLevel: number): IDepartment => {
  if (topLevel === 0 || topLevel === input.id) {
    return input
  }
  if (input.children.length === 0) {
    return emptyDepartment
  }
  return input.children.reduce((acc: IDepartment, value:IDepartment) => {
    const dept = filterOrgChartTree(value, topLevel);
    if (dept.id !== emptyDepartment.id) {
      acc = dept
    }
    return acc
  }, emptyDepartment);
};
