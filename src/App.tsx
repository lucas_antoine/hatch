// Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

import {css} from 'emotion';
import React, { Component} from 'react';
import OrgChartInput from './OrgChartInput';
import {IDepartment, emptyDepartment} from './Api';
import  jonsTreeData from './json/org-chart.json'


type InputOrgChartTestState = {
  loading: Boolean;
  orgChart: IDepartment;
  selected: IDepartment;
  topLevel: number,
};


export default class App extends Component<{}, InputOrgChartTestState> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true,
      orgChart: emptyDepartment,
      selected: emptyDepartment,
      topLevel: 0,
    };
  }

  updateSelection(newValue: string): void {
    this.setState(Object.assign(this.state, {selected: newValue}));
  }

  selectTopLevel(topLevel: number) {
    this.setState(Object.assign(this.state, {topLevel}) )
  }

  componentDidMount(): void {

    setTimeout(() => {
      this.setState({
        loading: false,
        orgChart: jonsTreeData.orgChart,
        selected: emptyDepartment,
      });
    }, 2000)
  }

  render() {
    return (
      <div className={styles.root}>

        <h1>Example App of an ugly ( but 'AAA'  (<a target="_blank" href="https://www.w3.org/WAI/standards-guidelines/wcag/https://www.w3.org/WAI/standards-guidelines/wcag/">A11Y</a>) orgchart input field</h1>

        <p>
          Selected value: <code>{JSON.stringify(this.state.selected)}</code>
        </p>

        <p>
          Choose the user's scope

          <br />
          <button onClick={() => this.selectTopLevel(0)}>
            All access (Can access any department)
          </button>

          <br />

          <button onClick={() => this.selectTopLevel(13)}>
            limited access: (Can only access the Quality department)
          </button>

        </p>
        {this.state.loading ? (
          'loading....'
        ) : (
          <OrgChartInput
            required
            tree={this.state.orgChart}
            onChange={(val: string) => this.updateSelection(val)}
            value={this.state.selected}
            level={0}
            topTree={this.state.topLevel}
          />
        )}
      </div>
    );
  }
}

const styles = {
  root: css`
    font-family: system-ui;
    margin: 24px auto;
    padding: 4px 24px 24px 24px;
    width: 800px;
  `,
};
