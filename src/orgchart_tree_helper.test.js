import test from 'ava';

import {filterOrgChartTree} from './orgchart_tree_helper';

import Api from './Api';

// import * as tree from './json/org-chart.json';


test('It Parses the tree correctly', () => {

  Api.getOrgChart().then((tree) => {
    const tree0 = filterOrgChartTree(tree, 0)

    expect(tree0).ToBe(tree)
  })

});
