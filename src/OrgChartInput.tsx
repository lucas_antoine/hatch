import React, {Component} from 'react';
import OrgChartItem, {OrgChartItemProps} from './OrgChartItem';
import {filterOrgChartTree} from './orgchart_tree_helper';
import {emptyDepartment} from './Api';



interface InputCommonProps {
  required: boolean
  label: string
}

interface OrgChartInputProps extends OrgChartItemProps, InputCommonProps {
  topTree: number
};


type InputPristineState = {
  pristine: boolean
}

export default class OrgChartInput extends Component<OrgChartInputProps, InputPristineState> {
  static defaultProps = {
    onChange: () => {
      console.warn("onChange function is not set - props won't update");
    },
    value: 0,
    required: false,
    label: 'Organisation department'
  };

  constructor(props: OrgChartInputProps) {
    super(props);
    this.state = {
      pristine: true
    }
  }

  componentDidMount(): void {

  }

  componentDidUpdate(prevProps: Readonly<OrgChartInputProps>, prevState: Readonly<InputPristineState>, snapshot?: any): void {
    if (prevState.pristine) {
      this.setState({pristine: false})
    }

  }

  render() {
    return (
      <div>
        <label>
          {this.props.label}
        <OrgChartItem
          tree={filterOrgChartTree(this.props.tree, this.props.topTree)}
          value={this.props.value}
          level={0}
          collapsed={false}
          onChange={this.props.onChange} />

          {this.props.required && !this.state.pristine && this.props.value.id===emptyDepartment.id ? <label className="error">this field is required</label> : null}
        </label>
      </div>

    );
  }
}
