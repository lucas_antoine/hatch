Hello, 



### Live Demo http://fec6abb5.ngrok.io/



To run it locally, you should just do:

```bash

yarn

# terminal 1 (it's actually using a json file)
cd server && node server.js

# terminal 2
npm run start


For the tests, see the section at the bottom....

```


First, you need to know that I had a contract at Squiz about 2.5 years ago, where I built for them an OrgChart for their major intranet platform - which is now in production and used by big corporations. It's been made in React/Redux.

![Org Chart](http://fec6abb5.ngrok.io/Squiz-DigitalWorkplace.png "Org Chart")

IMO, it looks better than Facebook Workplace's org chart. It was a 1 month tough job, with high coverage and Ie9 support.... *hint at how can I reproduce that effort in 4 hours.....*


Well, I decided to NOT spend more than 4 hours on this exercise and since it's been `2 years` I haven't wrote any React code - it took me about an hour to figure out how I was going to write this.

So it has no Redux, it is just an isolated component that basically takes 3 mandatory props:
 
    - The OrgChart Tree    
    - The Value    
    - an OnChange function - which will upate the parent container's prop.

then some optional props:
 
    - collapsed
    - label 
    - required

And of course, what's missing before it can make it to prod:

    - PropTypes maybe - is it necessary since we already got typescript....
    - One unit test for the tree filterring function
    - End2End tests with Cypress
    - Integration with the existing Form Validation architecture/implementation
    - Some design  
       
   
BTW, how long did it took you guys to setup a working react + typescript + Jest (or Ava) working environment from scratch?????
Cause it's been an hour that I have been fighting with Jest and Ava to actually parse Typescript + ES6 imports, and I kept having errors like, 
although I Googled it and tried their suggestions... but I keep getting this crap:
```
  Details:

    /home/al/code/redux-react-hook/src/Api.ts:16
      static cache = {};
                   ^

    SyntaxError: Unexpected token =

    > 1 | import {IDepartment, emptyDepartment} from './Api';
        | ^
      2 | 
      3 | 
      4 | export const filterOrgChartTree = (input : IDepartment, topLevel: number): IDepartment => {

      at ScriptTransformer._transformAndBuildScript (node_modules/@jest/transform/build/ScriptTransformer.js:537:17)
      at ScriptTransformer.transform (node_modules/@jest/transform/build/ScriptTransformer.js:579:25)
      at Object.<anonymous> (src/orgchart_tree_helper.ts:1:1)

```


If you want to see my testing skills, please head to https://github.com/ng-consult/redis-url-cache/tree/master/test - it's a node/typescript Redis based url caching library I wrote 3 years ago (for fun).

My actual view on unit testing:
===============================

I am not a fan of unit testing for frontend, I'd rather have a good 80% coverage on backend than frontend, and get as much business logic on the backend.
It is actually easier on the backend to implement business logics, 

Instead I would focus the tests almost exclusively on E2E  - my personal preference would be on Cypress https://www.cypress.io/

So I wanted to use Cypress, but again, i didn't want to spend more than 4 hours on this task - and since I already lost an hour 
(without success) trying to get jest or Ava to work with typescript to test this function `const filterOrgChartTree = (input : IDepartment, topLevel: number): IDepartment `, and another hour refreshing my React skills.

So that's all you will get, I am sorry if that disappoint you.

It still works fine though.

### PostGresql

There are two solutions, both with pro and cons,

One normalized solution, making the initial retrieval of the whole tree a bit of a pain
- PRO: It is easier to edit/delete/move a tree node
- CON: For a bit corporation with as much as 20 levels, we'd need a 20 LEFT joins query on top of the result parsing into JSON.
 
One normalized JsonB solution, where the tree is stored into a JSONB field.
- PRO: Well, initial read is easy as it gets
- CONS: Edition of the tree is a bit more tricky, and some map/reduce work might need to be done to extract the data..

I would reckon a mixed alternative, keeping the normalized structure, but a bit more flattened, for example:

### Organizations:
id PK

name

root_tree_id INT foreign key Restrict on deleted


### Departements
id PK

parent_id FK(id) restrict on delete

name string

organization_id FK(organization(id)) //!!=> This is the flattening part



### User table (could also be a Car/Computer table)
id PK

organization_id FK

department_id FK




This way, it's possible to do something like this:

```sql

SELECT id, parent_id, name from departments where organization_id=123


Then we just need to do two passes, one indexing by parent_id, and the second one to build the tree, complexity 2N

//  tmpTree = map[id][]row{id, name, parent_id}

const tmpTree = {};
const tree = {};
rows.forEach(() => {
    if (typeof tmptree[row.parent_id] === 'undefined') {
        tmptree[row.parent_id] = [];
    } 
    tmptree[row.parent_id].push(row)

    if (row.parent_id === 0) {
        // this is the root
        
        tree = {
              id: row.id, 
              name: row.name,
              children: [],
        }
    }
}

const extract = (subTree) {
   subTree.children = tmpTree[subTree.id].map((row) => {
       return extract({id: row.id, name: row.name, children:[]})       
    });   
}

extract(tree);

// untested - buut *Should* work.

```
